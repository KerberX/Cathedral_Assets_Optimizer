<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>MainWindow</name>
    <message>
        <source>Cathedral Assets Optimizer</source>
        <translation>Cathedral Assets Optimizer</translation>
    </message>
    <message>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>ファイルへ実際の書き込みは行わず、処理だけを行います</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>ディレクトリを開く</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>実行</translation>
    </message>
    <message>
        <source>One mod</source>
        <translation>一つのMod</translation>
    </message>
    <message>
        <source>Dry run</source>
        <translation>テスト実行</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>詳細設定を表示する</translation>
    </message>
    <message>
        <source>SSE</source>
        <translation>SSE</translation>
    </message>
    <message>
        <source>BSA</source>
        <translation>BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ModフォルダにあるBSAをすべて抽出します。 &lt;span style=&quot; font-weight:600;&quot;&gt;警告&lt;/span&gt;: このオプションを有効にした場合、処理がかなり遅くなります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Extract BSA</source>
        <translation>BSAを抽出</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;デフォルトでは、既存のBSAはバックアップが作成されます。このオプションを有効にすると、バックアップを行いません。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Delete backups </source>
        <translation>バックアップを削除 </translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;既存のルーズファイルをパックして、新しいBSAを作成します。&lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;警告&lt;/span&gt;:  このオプションを有効にした場合、処理がかなり遅くなります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create BSA</source>
        <translation>BSAを作成</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>詳細</translation>
    </message>
    <message>
        <source>Morrowind</source>
        <translation>Morrowind</translation>
    </message>
    <message>
        <source>Oblivion</source>
        <translation>Oblivion</translation>
    </message>
    <message>
        <source>Skyrim / Fallout 3 / Fallout New Vegas</source>
        <translation>Skyrim / Fallout 3 / Fallout New Vegas</translation>
    </message>
    <message>
        <source>Fallout 4</source>
        <translation>Fallout 4</translation>
    </message>
    <message>
        <source>Maximum size</source>
        <translation>最大サイズ</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation>エクステンション</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation>接尾(サフィックス)</translation>
    </message>
    <message>
        <source>Meshes</source>
        <translation>メッシュ</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;確実にゲームをクラッシュさせるメッシュの修復を試みます。 これにはヘッドパーツが含まれています。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Necessary optimization</source>
        <translation>必ず必要な最適化</translation>
    </message>
    <message>
        <source>Medium optimization</source>
        <translation>中程度の最適化</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;すべてのメッシュを完全に最適化します。 本来はメッシュの最適化が必要なファイルなのに無視されてしまった場合にだけ適用します。 表示品質が低下する可能性があります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Full optimization</source>
        <translation>すべて最適化</translation>
    </message>
    <message>
        <source>Always process headparts</source>
        <translation>常にヘッドパーツを処理する</translation>
    </message>
    <message>
        <source>Resave meshes</source>
        <translation>メッシュを再保存する</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <source>Oblivion (V20_0_0_5)</source>
        <translation>Oblivion (V20_0_0_5)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</translation>
    </message>
    <message>
        <source>User</source>
        <translation>ユーザー</translation>
    </message>
    <message>
        <source>Oblivion (11)</source>
        <translation>Oblivion (11)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</translation>
    </message>
    <message>
        <source>Stream</source>
        <translation>ストリーム</translation>
    </message>
    <message>
        <source>Oblivion / Fallout 3 (82)</source>
        <translation>Oblivion / Fallout 3 (82)</translation>
    </message>
    <message>
        <source>Skyrim (83)</source>
        <translation>Skyrim (83)</translation>
    </message>
    <message>
        <source>Skyrim SE (100)</source>
        <translation>Skyrim SE (100)</translation>
    </message>
    <message>
        <source>Fallout 4 (130)</source>
        <translation>Fallout 4 (130)</translation>
    </message>
    <message>
        <source>Textures</source>
        <translation>テクスチャ</translation>
    </message>
    <message>
        <source>Resizing</source>
        <translation>サイズ変更</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>高さ:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>幅:</translation>
    </message>
    <message>
        <source>By ratio</source>
        <translation>割合で</translation>
    </message>
    <message>
        <source>By fixed size</source>
        <translation>固定サイズで</translation>
    </message>
    <message>
        <source>TGA</source>
        <translation>TGA</translation>
    </message>
    <message>
        <source>Enable TGA conversion</source>
        <translation>TGAの変換を有効にする</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>プロファイル</translation>
    </message>
    <message>
        <source>New profile</source>
        <translation>新しいプロファイル</translation>
    </message>
    <message>
        <source>New</source>
        <translation>新規</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;実際には処理をせず、変更されるすべてのファイルを一覧化します。BSA内のファイルは無視します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Several mods </source>
        <translation>複数のMod </translation>
    </message>
    <message>
        <source>Process BSAs</source>
        <translation>BSAの処理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ファイルの最大非圧縮サイズ(GB)。これを増やすと、BSAごとに保存されるファイルの数が増えますが、BSAのサイズが仕様上の制限に達し、ゲームがクラッシュする可能性があります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&quot;.bsa&quot;の拡張子。通常これはSkyrim向けです。&quot;.ba2&quot;は、Fallout 4向けです。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;例: &quot;- Main.ba2&quot; はFallout 4向けです。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create textures BSA</source>
        <translation>テクスチャBSAを作成</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;例: &quot;- Textures.bsa&quot; はSkyrim SE向けです。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>エキスパート</translation>
    </message>
    <message>
        <source>BSA Format</source>
        <translation>BSAの形式</translation>
    </message>
    <message>
        <source>Textures BSA Format</source>
        <translation>テクスチャBSAの形式</translation>
    </message>
    <message>
        <source>Process meshes</source>
        <translation>メッシュの処理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;必要な最適化を実行し、他のメッシュを軽く最適化します。 これにより、絵的な問題が修正される場合もありますが、品質が低下することがあります。 &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;このオプションを無効にすると、ヘッドパーツは無視されます。 複数のModを処理するときは、無効にしておくことを推奨します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;最適化が無効になっている場合でも、メッシュを再保存します。 NifSkopeでメッシュを開くのと同じ最適化を実行します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Process textures</source>
        <translation>テクスチャの処理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;SSEはTGAファイル読み取ることができないため、DDSへ変換します。 一部の古い形式はSSEと互換性がないため、ゲームをクラッシュさせるテクスチャを変換および修正しようとします。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;圧縮されていないテクスチャを設定された出力形式で圧縮します。 SSEおよびFO4では推奨できますが、他のゲームでは品質を大幅に低下させる可能性があります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Compress textures</source>
        <translation>テクスチャを圧縮する</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;テクスチャのミップマップを生成し、ディスクとVRAMの使用量を増やしてパフォーマンスを改善します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Generate mipmaps</source>
        <translation>ミップマップを生成する</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;テクスチャのサイズを変更し、現在の幅と高さを指定された値で除算します。4x4以上で2の累乗で表現されるテクスチャの寸法を指定する必要があります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;テクスチャのサイズを指定されたサイズに変更します。4x4以上の2の累乗で表現されるテクスチャの寸法を指定する必要があります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;一部のゲームは、圧縮されたインターフェイスのテクスチャをサポートしていません。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;非圧縮テクスチャ、変更されたテクスチャ、および変換されたTGAファイルの出力形式。 BC7は、SSEまたはFO4にのみ使用してください。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Output format</source>
        <translation>出力形式</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;オプションが選択されている場合、TGAを互換性のあるDDS形式に変換します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>BC7 (BC7_UNORM)</source>
        <translation>BC7 (BC7_UNORM)</translation>
    </message>
    <message>
        <source>BC3 (BC3_UNORM)</source>
        <translation>BC3 (BC3_UNORM)</translation>
    </message>
    <message>
        <source>BC1 (BC1_UNORM)</source>
        <translation>BC1 (BC1_UNORM)</translation>
    </message>
    <message>
        <source>Uncompressed (R8G8B8A8_UNORM)</source>
        <translation>圧縮しない (R8G8B8A8_UNORM)</translation>
    </message>
    <message>
        <source>Process animations</source>
        <translation>アニメーションの処理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;アニメーションを適切な形式に変換します。 アニメーションに既に互換性がある場合、変更は行われません。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <source>Enable debug log</source>
        <translation>デバッグ ログを有効にする</translation>
    </message>
    <message>
        <source>Used when reporting bugs</source>
        <translation>バグレポートのときに使用</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>ドキュメンテーション</translation>
    </message>
    <message>
        <source>Discord</source>
        <translation>Discord</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Cathedral Assets Optimizerについて</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>Qtについて</translation>
    </message>
    <message>
        <source>Show tutorials</source>
        <translation>チュートリアルを表示</translation>
    </message>
    <message>
        <source>Open log file</source>
        <translation>ログ ファイルを開く</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>インターフェース</translation>
    </message>
    <message>
        <source>Compress interface textures</source>
        <translation>インターフェースのテクスチャを圧縮する</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;これらの形式を使用しているテクスチャは、サポートされている形式に変換されます。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unwanted formats</source>
        <translation>望まれない形式</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <source>Animations</source>
        <translation>アニメーション</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>ログ</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>ツール</translation>
    </message>
    <message>
        <source>Enable dark theme</source>
        <translation>ダークテーマを有効にする</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation>詳細設定</translation>
    </message>
    <message>
        <source>Advanced settings can only be modified when using custom profiles.</source>
        <translation>詳細設定は、カスタムプロファイルを使用する場合にのみ変更できます。</translation>
    </message>
    <message>
        <source>Several mods option</source>
        <translation>複数のMod向けオプション</translation>
    </message>
    <message>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </source>
        <translation>いくつかのModオプションが選択されています。特にBSAの処理をする場合、非常に長い時間がかかることがあります。</translation>
    </message>
    <message>
        <source>This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>この処理は、Mod Organizer の mods フォルダーでだけテストされています。</translation>
    </message>
    <message>
        <source>
Made by G&apos;k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</source>
        <translation>
製作者 by G&apos;k
このプログラムは、有用であることを期待して配布されていますが、何の保証もありません。Mozilla Public Licenseをご覧ください</translation>
    </message>
    <message>
        <source>Save unsaved changes</source>
        <translation>未保存の変更を保存する</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</source>
        <translation>未保存の変更があります。それらを保存しますか？ &apos;すべてはい&apos; を押して、未保存の変更をすべて保存することもできます</translation>
    </message>
    <message>
        <source>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</source>
        <translation>新しいプロファイルを作成しようとしています。&apos;CAO/profiles&apos; に新しいディレクトリが作成されます。作成後にチェックしてみてください。いくつかのファイルが作成されているはずです。</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>名前:</translation>
    </message>
    <message>
        <source>Base profile</source>
        <translation>ベース プロファイル</translation>
    </message>
    <message>
        <source>Which profile do you want to use as a base?</source>
        <translation>どのプロファイルをベースとして使用しますか？</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <source>An exception has been encountered and the process was forced to stop: </source>
        <translation>例外が発生し、処理は強制的に停止されました: </translation>
    </message>
    <message>
        <source>Done</source>
        <translation>完了</translation>
    </message>
    <message>
        <source>Welcome to %1 %2</source>
        <translation>ようこそ %1 %2 へ</translation>
    </message>
    <message>
        <source>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</source>
        <translation>CAOを初めて実行しているようです。すべてのオプションには、その機能を説明するツールチップがあります。ヘルプが必要な場合は、Discordに参加することもできます。ダークテーマも使用できます。</translation>
    </message>
    <message>
        <source>Save UI</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TexturesFormatSelectDialog</name>
    <message>
        <source>Dialog</source>
        <translation>ダイアログ</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>検索</translation>
    </message>
</context>
</TS>
